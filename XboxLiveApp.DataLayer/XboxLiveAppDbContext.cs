﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using XboxLiveApp.DataLayer.Models;

namespace XboxLiveApp.DataLayer
{
    public interface IXboxLiveAppDbContext:IDisposable
    {
        DbSet<Users> Users { get; set; }
        DbSet<Game> Games { get; set; }
        DbSet<SellDetails> SellDetails { get; set; }
        DbSet<FriendRequest> Requests { get; set; }

        DbSet<Achievement> Achievements { get; set; }
        DatabaseFacade Database { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }

    public class XboxLiveAppDbContext : DbContext, IXboxLiveAppDbContext
    {
        private const string _connectionString = "Data Source=.;Initial Catalog=XboxLiveDb;Integrated Security=True;";

        public DbSet<Users> Users { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<SellDetails> SellDetails { get; set; }
        public DbSet<FriendRequest> Requests { get; set; }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
