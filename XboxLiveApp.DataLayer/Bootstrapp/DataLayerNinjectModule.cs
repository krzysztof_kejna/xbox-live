﻿using System;
using System.Collections.Generic;
using System.Text;
using Ninject.Modules;

namespace XboxLiveApp.DataLayer.Bootstrapp
{
    public class DataLayerNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IXboxLiveAppDbContext>().ToMethod(x => new XboxLiveAppDbContext());
        }
    }
}
