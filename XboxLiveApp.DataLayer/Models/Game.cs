﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XboxLiveApp.DataLayer.Models
{
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int AgeRestriction { get; set; }
        public bool GamePass { get; set; }
    }
}
