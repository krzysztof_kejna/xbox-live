﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XboxLiveApp.DataLayer.Models
{
    public class Users
    {
        public int Id { get; set; }

        public string Nick { get; set; }

        public string Email { get; set; } 

        public DateTime? DOB { get; set; }

        public string Password { get; set; }
        public bool GPS { get; set; }

    }
}
