﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XboxLiveApp.DataLayer.Models
{
    public class SellDetails
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public Users User { get; set; }
        public int GameId { get; set; }
        public Game Game { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

    }
}
