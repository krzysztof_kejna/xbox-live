﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XboxLiveApp.DataLayer.Models
{
    public class Achievement
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool AchievedByUser { get; set; }
        public int Score { get; set; }
        public Game Game { get; set; }
        public int? UserId { get; set; }
        public Users User { get; set; }
    }
}
