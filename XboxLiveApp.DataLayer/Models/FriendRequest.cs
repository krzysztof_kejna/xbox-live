﻿using System;

namespace XboxLiveApp.DataLayer.Models
{
    public class FriendRequest
    {
        public int Id { get; set; }

        public Users Sender { get; set; }

        public Users Reciever { get; set; }

        public DateTime InvitationDate { get; set; }

        public bool? IsActive { get; set; }

    }
}