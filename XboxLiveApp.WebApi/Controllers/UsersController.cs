﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using XboxLiveApp.BusinnesLayer.Services;
using XboxLiveApp.DataLayer.Models;
using XboxLiveApp.WebApi.Bootstrapp;

namespace XboxLiveApp.WebApi.Controllers
{
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;

        public UsersController()
        {
            var kernel = DependencyResolver.GetKernel();

            _userService = kernel.Get<IUserService>();
            _emailService = kernel.Get<IEmailService>();
        }

        [HttpGet("{id}")]
        public async Task<Users> GetUserByIdAsync(int id)
        {
            var user = await _userService.GetUserById(id);
            return user;
        }

        [HttpGet("{id}/GamePassActive")]
        public async Task BuyGamePassAsync(int id, [FromBody]Users user)
        {
            user.Id = id;
            user.GPS = true;
            await _userService.GamePassForUser(user);
            _emailService.SendEmail(user.Email, "Activation of your gamepass subscription", "This email is confirmation of your game pass activation");
        }

        [HttpGet("{id}/GamePassCancel")]
        public async Task CancelGamePassAsync(int id, [FromBody]Users user)
        {
            user.Id = id;
            user.GPS = false;
            await _userService.GamePassForUser(user);
            _emailService.SendEmail(user.Email, "Canceling your gamepass subscription", "This email is confirmation of your game pass cancel");
        }

        [HttpPost]
        public async Task<int> PostUser([FromBody]Users user)
        {
            var userId = await _userService.AddUser(user);
            return userId;
        }
    }
}
