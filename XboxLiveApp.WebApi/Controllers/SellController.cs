﻿using Microsoft.AspNetCore.Mvc;
using Ninject;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using XboxLiveApp.BusinnesLayer.Services;
using XboxLiveApp.WebApi.Bootstrapp;
using XboxLiveApp.DataLayer.Models;

namespace XboxLiveApp.WebApi.Controllers
{
    [Route("api/sell")]

    public class SellController : ControllerBase
    {
        private ISellService _sellService;
        private IEmailService _emailService;
        private IUserService _userService;
        private IGameService _gameService;

        public SellController()
        {
            var kernel = DependencyResolver.GetKernel();

            _sellService = kernel.Get<ISellService>();
            _emailService = kernel.Get<IEmailService>();
            _userService = kernel.Get<IUserService>();
            _gameService = kernel.Get<IGameService>();
        }

        [HttpGet ("{userId}/{gameId}")]
        public async Task<int> PostSell(int userId, int gameId)
        {
            var sellId = await _sellService.SellGame(
                userId,
                gameId);
            var user = await _userService.GetUserById(userId);
            var game = await _gameService.GetGameById(gameId);

            await _emailService.SendEmail(user.Email, "You have both game on XboxLive", $"You have bought: {game.Name} for {game.Price}");
            return sellId;
        }
    }
}
