﻿using Microsoft.AspNetCore.Mvc;
using Ninject;
using System.Threading.Tasks;
using XboxLiveApp.BusinnesLayer.Services;
using XboxLiveApp.DataLayer.Models;
using XboxLiveApp.WebApi.Bootstrapp;

namespace XboxLiveApp.WebApi.Controllers
{
    [Route("api/friendrequest")]
    internal class FriendRequestController : ControllerBase

    {
        private readonly IFriendRequestService _friendRequestService;

        public FriendRequestController()
        {
            var kernel = DependencyResolver.GetKernel();

            _friendRequestService = kernel.Get<IFriendRequestService>();
        }

        [HttpPost]
        public async Task<int> PostFriendRequest([FromBody]FriendRequest request)
        {
            var userId = await _friendRequestService.AddRequest(request);
            return userId;
        }

        [HttpGet("{id}")]
        public async Task GetFriendRequests(string nick, int id, [FromBody] FriendRequest request)
        {
            request.Sender.Nick = nick;
            request.Id = id;
            request.IsActive = true;
            _friendRequestService.GetRequests(nick);
        }
    }
}