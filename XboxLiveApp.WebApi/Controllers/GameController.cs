﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using XboxLiveApp.BusinnesLayer.Services;
using XboxLiveApp.DataLayer.Models;
using XboxLiveApp.WebApi.Bootstrapp;

namespace XboxLiveApp.WebApi.Controllers
{
    [Route("api/game")]
    public class GameController : ControllerBase
    {
        private readonly IGameService _gameService;

        public GameController()
        {
            var kernel = DependencyResolver.GetKernel();
            _gameService = kernel.Get<IGameService>();
        }

        [HttpGet("{userId}/{id}")]
        public async Task<string> GetGameAvabilty(int userId, int id)
        {
            var gameAva = await _gameService.IsGameAvalibleForUser(userId, id);
            return gameAva;
        }
    }
}

