﻿using Microsoft.AspNetCore.Mvc;
using Ninject;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using XboxLiveApp.BusinnesLayer.Services;
using XboxLiveApp.WebApi.Bootstrapp;
using XboxLiveApp.DataLayer.Models;

namespace XboxLiveApp.WebApi.Controllers
{
    [Route("api/a")]

    public class AchievementController : ControllerBase
    {
        private IAchievementService _achievementService;
        private IEmailService _emailService;
        private IUserService _userService;
        private IGameService _gameService;

        public AchievementController()
        {
            var kernel = DependencyResolver.GetKernel();

            _achievementService = kernel.Get<IAchievementService>();
            _emailService = kernel.Get<IEmailService>();
            _userService = kernel.Get<IUserService>();
            _gameService = kernel.Get<IGameService>();
        }

        [HttpGet("{userId}/{id}")]
        public async Task BuyGamePassAsync(int id, int userId)
        {
            await _achievementService.ChangeAchievmentStatus(id, userId);
        }
    }
}
