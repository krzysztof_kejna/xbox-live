﻿using System;
using XboxLiveApp.WebApi.Bootstrapp;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Topshelf;

namespace XboxLiveApp.WebApi
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var host = HostFactory.Run(config =>
            {
                config.Service<AppHost>(s =>
                {
                    s.ConstructUsing(_ => new AppHost());
                    s.WhenStarted(appHost => appHost.Start());
                    s.WhenStopped(appHost => appHost.Stop());
                });

                config.RunAsLocalSystem();

                config.SetServiceName("XboxLiveApp Service");
                config.SetDisplayName("XboxLiveApp Service");
                config.SetDescription("XboxLiveApp hosted as a Windows Service");
            });

            var exitCode = (int)Convert.ChangeType(host, host.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}