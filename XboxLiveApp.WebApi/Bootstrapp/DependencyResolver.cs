﻿using System;
using System.Collections.Generic;
using System.Text;
using XboxLiveApp.BusinnesLayer.Bootstrapp;
using Ninject;
using Ninject.Modules;
using XboxLiveApp.DataLayer.Bootstrapp;

namespace XboxLiveApp.WebApi.Bootstrapp
{
    public static class DependencyResolver
    {
        private static IKernel _kernel = null;

        public static IKernel GetKernel()
        {
            if (_kernel != null)
            {
                return _kernel;
            }

            _kernel = new StandardKernel();

            _kernel.Load(new INinjectModule[]
            {
                new BusinessNinjectModule(),
                new DataLayerNinjectModule()
            });

            return _kernel;
        }
    }
}
