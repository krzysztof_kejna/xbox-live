﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace XboxLiveApp.WebApi.Bootstrapp
{
    internal class AppHost
    {
        private IWebHost _webHost;
        public void Start()
        {
            _webHost = WebHost.CreateDefaultBuilder()
                .ConfigureLogging(ConfigureLogging)
                .ConfigureServices(services =>
                {
                    services.AddMvc();
                    services.AddSwaggerGen(SwaggerDocsConfig);
                })
                .Configure(app =>
                {
                    app.UseMvc();
                    app.UseCors();
                    app.UseSwagger();
                    app.UseSwaggerUI(c =>
                    {
                        c.SwaggerEndpoint("/swagger/v1/swagger.json", "XboxLiveApp API V1");
                        c.RoutePrefix = string.Empty;
                    });
                })
                .UseUrls("http://*:10500")
                .Build();

            _webHost.Start();
        }

        private static void ConfigureLogging(WebHostBuilderContext hostingContext, ILoggingBuilder logging)
        {
            logging.ClearProviders();
        }

        private void SwaggerDocsConfig(SwaggerGenOptions genOptions)
        {
            genOptions.SwaggerDoc(
                "v1",
                new Info
                {
                    Version = "v1",
                    Title = "XboxLiveApp",
                    Description = "XboxLiveApp API v1",
                    TermsOfService = "https://xboxliveapp.project.com/terms",
                    Contact = new Contact
                    {
                        Name = "Codementors group",
                        Email = "Codementors@xboxliveapp.pl",
                    },
                    License = new License
                    {
                        Name = "Use some license",
                        Url = "https://carsharing.project.com/license"
                    }
                });
            var xmlFile = Path.ChangeExtension(typeof(Program).Assembly.Location, ".xml");
            genOptions.IncludeXmlComments(xmlFile);
        }

        public void Stop()
        {
            _webHost.StopAsync();
        }
    }
}

