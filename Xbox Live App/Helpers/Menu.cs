﻿using System;
using System.Collections.Generic;

namespace Xbox_Live_App.Helpers
{
    public interface IMenu
    {
        void AddMenuItem(Action action, string description, int key);

        void ExecuteAction(int key);

        void ShowMenu();
    }

    public class Menu : IMenu
    {
        private Dictionary<int, MenuItem> _menuItems = new Dictionary<int, MenuItem>();

        public void AddMenuItem(Action action, string description, int key)
        {
            if (_menuItems.ContainsKey(key))
            {
                throw new Exception($"There is already menu named {key}");
            }
            _menuItems.Add(
                key,
                new MenuItem { Action = action, Description = description });
        }

        public void ExecuteAction(int key)
        {
            if (!_menuItems.ContainsKey(key))
            {
                throw new Exception($"There is no defined action for {key}");
            }
            _menuItems[key].Action();
        }

        public void ShowMenu()
        {
            Console.WriteLine("Available options:");
            foreach (var item in _menuItems)
            {
                Console.WriteLine($"\t{item.Key} {item.Value.Description}");
            }
        }
    }
}