﻿using System;

namespace Xbox_Live_App.Helpers
{
    internal class MenuItem
    {
        public string Description;
        public Action Action;
    }
}