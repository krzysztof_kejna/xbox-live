﻿using System;
using System.Globalization;

namespace Xbox_Live_App.Helpers
{
    internal interface IIoHelper
    {
        DateTime? GetDateFromUser(string message);

        int GetIntFromUser(string message);
        double GetDoubleFromUser(string message);

        string GetStringFromUser(string message);
    }

    internal class IoHelper : IIoHelper
    {
        public string GetStringFromUser(string message)
        {
            Console.Write($"{message}");
            return Console.ReadLine();
        }

        public int GetIntFromUser(string message)
        {
            int result = 0;
            var success = false;

            while (!success)
            {
                Console.WriteLine(message);
                var input = Console.ReadLine();
                success = int.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It's not a valid number");
                }
            }
            return result;
        }
        public double GetDoubleFromUser(string message)
        {
            double result = 0.0;
            var success = false;

            while (!success)
            {
                Console.WriteLine(message);
                var input = Console.ReadLine();
                success = double.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It's not a valid number");
                }
            }
            return result;
        }

        public DateTime? GetDateFromUser(string message)
        {
            const string dateFormat = "dd-MM-yyyy";
            var result = new DateTime();
            var success = false;

            while (!success)
            {
                Console.Write($"{message} ({dateFormat}): ");
                var input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input) == false)
                {
                    return null;
                }

                success = DateTime.TryParseExact(
                    input,
                    dateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out result);

                if (!success)
                {
                    Console.WriteLine("Incorrect date format. Enter date in dd-MM-yyyy format. ");
                }
            }
            return result;
        }
    }
}