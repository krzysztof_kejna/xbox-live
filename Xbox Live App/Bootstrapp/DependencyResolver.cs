﻿using System;
using System.Collections.Generic;
using System.Text;
using Ninject;
using Ninject.Modules;
using XboxLiveApp.BusinnesLayer.Bootstrapp;
using XboxLiveApp.DataLayer.Bootstrapp;

namespace Xbox_Live_App.Bootstrapp
{
    public static class DependencyResolver
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel();

            kernel.Load(new INinjectModule[]
            {
                new CliNinjectModule(), 
                new BusinessNinjectModule(), 
                new DataLayerNinjectModule() 
            });

            return kernel;
        }
    }
}
