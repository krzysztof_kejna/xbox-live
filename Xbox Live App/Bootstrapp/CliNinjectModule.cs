﻿using System;
using System.Collections.Generic;
using System.Text;
using Ninject.Modules;
using Xbox_Live_App.Helpers;

namespace Xbox_Live_App.Bootstrapp
{
    public class CliNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IProgram>().To<Program>();
            Kernel.Bind<IIoHelper>().To<IoHelper>();
            Kernel.Bind<IMenu>().To<Menu>();
        }
    }
}
