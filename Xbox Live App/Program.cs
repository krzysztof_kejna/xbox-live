﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Ninject;
using Xbox_Live_App.Bootstrapp;
using Xbox_Live_App.Helpers;
using XboxLiveApp.BusinnesLayer.Bootstrapp;
using XboxLiveApp.BusinnesLayer.Services;
using XboxLiveApp.DataLayer.Models;

namespace Xbox_Live_App
{
    internal interface IProgram
    {
        void Run();
    }

    internal class Program : IProgram
    {
        private readonly IUserService _userService;
        private readonly IGameService _gameService;
        private readonly IEmailService _emailService;
        private readonly IAchievementService _achievementService;
        private readonly ISellService _sellService;
        private readonly IIoHelper _ioHelper;
        private readonly IDatabaseInitializer _databaseInitializer;
        private readonly IMenu _menu;

        public Program(
            IUserService userService,
            IGameService gameService,
            IEmailService emailService,
            IAchievementService achievementService,
            ISellService sellService,
            IIoHelper ioHelper,
            IDatabaseInitializer databaseInitializer,
            IMenu menu)
        {
            _userService = userService;
            _gameService = gameService;
            _emailService = emailService;
            _achievementService = achievementService;
            _sellService = sellService;
            _ioHelper = ioHelper;
            _databaseInitializer = databaseInitializer;
            _menu = menu;
        }

        private static void Main(string[] args)
        {
            using (var kernel = DependencyResolver.GetKernel())
            {
                var program = kernel.Get<IProgram>();
                program.Run();
            }
        }

        public void Run()
        {
            DBInitialize();
            InitializeMenu();
            while (true)
            {
                _menu.ShowMenu();
                var userInput = _ioHelper.GetIntFromUser($"Enter command number to select menu option: ");
                try
                {
                    _menu.ExecuteAction(userInput);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Command execution failed {e.Message}");
                }
            }
        }

        private void InitializeMenu()
        {
            _menu.AddMenuItem(AddGame, "Add game", 2);
            _menu.AddMenuItem(DisplayUsers, "Display registered users", 3);
            _menu.AddMenuItem(DisplayGames, "Display all games", 4);
            _menu.AddMenuItem(AddAchievement, "Add achievement", 6);
            _menu.AddMenuItem(DisplayListOfActiveGamePassSubscribers, "Display list of active game pass subscribers", 7);
            _menu.AddMenuItem(DisplayHighScoreList, "Display High Score list", 8);
            _menu.AddMenuItem(AddGameToGamePass, "Add game to gamepass", 9);

            _menu.AddMenuItem(Exit, "Exit", 30);
        }
        private void DBInitialize()
        {
            _databaseInitializer.Initialize();
        }

        private void DisplayGames()
        {
            foreach (var game in _gameService.GetGames().Result)
            {
                var score = _achievementService.GetAchievmentScoreForGame(game.Id).Result;
                DisplaySingleGame(game, score);
            }
            Console.ReadKey();
        }

        private void DisplaySingleGame(Game game, int score)
        {
            Console.WriteLine($"Game name: {game.Name} game price: {game.Price} age restriction: {game.AgeRestriction}, total achievment points:{score}");        
        }

        private void DisplayUsers()
        {
            foreach (var user in _userService.GetUsers().Result)
            {
                DisplaySingleUser(user);
            };
            Console.ReadKey();
        }

        private void DisplaySingleUser(Users user)
        {
            Console.WriteLine($"user nick: {user.Nick} Email: {user.Email} Date of birth: {user.DOB.Value.ToShortDateString()}\n");
        }

        private void CreateUser()
        {
            Console.WriteLine("Creating user");

            var newUser = new Users
            {
                Nick = _ioHelper.GetStringFromUser("Enter nick"),
                Email = _ioHelper.GetStringFromUser("Enter email"),
                DOB = _ioHelper.GetDateFromUser("Enter DOB in dd-MM-yyyy format: "),
                Password = _ioHelper.GetStringFromUser("Enter password: ")
            };
            _userService.AddUser(newUser);
        }

        private void AddGame()
        {
            Console.WriteLine("Add game:");

            var newGame = new Game
            {
                Name = _ioHelper.GetStringFromUser("Enter name:"),
                Price = _ioHelper.GetDoubleFromUser("Enter price:"),
                AgeRestriction = _ioHelper.GetIntFromUser("Enter age category:"),
                GamePass = false
            };

            var addGame = _gameService.AddGame(newGame).Result;
            Console.WriteLine($"game created. Id = {addGame}");
        }

        private void AddAchievement()
        {
            Console.WriteLine("Adding Achievement.");

            var gameId = _ioHelper.GetIntFromUser("Enter game id");

            var choosenGame = _gameService.GetGameById(gameId).Result;

            var newAchievement = new Achievement
            {
                Name = _ioHelper.GetStringFromUser("Enter name: "),
                Description = _ioHelper.GetStringFromUser("Enter description: "),
                Score = _ioHelper.GetIntFromUser("Enter score value: "),
                AchievedByUser = false,
                GameId = gameId,
                UserId = null
            };

            _achievementService.Add(newAchievement);

            foreach (var user in _sellService.GetAllUsersThatBoughtTheGame(gameId).Result)
            {
                var achievement = new Achievement
                {
                    Name = newAchievement.Name,
                    Description = newAchievement.Description,
                    Score = newAchievement.Score,
                    AchievedByUser = false,
                    GameId = gameId,
                    UserId = user.UserId
                }; ;
                achievement.UserId = user.UserId;
                _achievementService.Add(achievement);
            }
            Console.WriteLine($"{newAchievement.Name} achievement added to {choosenGame.Name}");
        }

        private void DisplayListOfActiveGamePassSubscribers()
        {
            Console.WriteLine("Active subscribers:");

            var activeSubscribers = _userService.GetUsersWithActiveGamePass().Result;

            foreach (var subscriber in activeSubscribers)
            {
                Console.WriteLine($"{subscriber.Id}, {subscriber.Nick}, {subscriber.Email} {subscriber.DOB.Value.ToShortDateString()}");
            }

            Console.ReadLine();
        }

        private void DisplayHighScoreList()
        {
            var highScoreList = new Dictionary<string, int>();
            var allUserList = _userService.GetUsers().Result;
            foreach (var user in allUserList)
            {
                var achievmentScore = _achievementService.ReturnAllAchieventsAchievedForUser(user.Id).Result.Sum(s => s.Score);
                highScoreList.Add(user.Nick, achievmentScore);
            }

            var result = highScoreList.OrderByDescending(v => v.Value);

            foreach (var item in result)
            {
                Console.WriteLine($"{item.Key}, {item.Value.ToString()}");
            }
        }

        private void AddGameToGamePass()
        {
            Console.WriteLine("Add game to game pass");
            var input = _ioHelper.GetIntFromUser("Type game id");
            var game = _gameService.GetGameById(input).Result;
            
            if (game.GamePass == true)
            {
                Console.WriteLine("This game is already in gamepass");
                return;
            }

            game.GamePass = true;
            _gameService.ModifyGame(game);
        }
        private void Exit()
        {
            Console.WriteLine("Thank you for visiting this app! Press any key to close.");
            Environment.Exit(0);
        }
    }
}