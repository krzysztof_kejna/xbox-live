﻿using System;
using System.Collections.Generic;
using System.Text;
using XboxLiveApp.DataLayer;

namespace XboxLiveApp.BusinnesLayer.Bootstrapp
{
    public interface IDatabaseInitializer
    {
        void Initialize();
    }

    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly Func<IXboxLiveAppDbContext> _dbContextFactory;

        public DatabaseInitializer(Func<IXboxLiveAppDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public void Initialize()
        {
            using (var ctx = _dbContextFactory())
            {
                ctx.Database.EnsureCreated();
            }
        }
    }
}
