﻿using System;
using System.Collections.Generic;
using System.Text;
using Ninject.Modules;
using XboxLiveApp.BusinnesLayer.Services;

namespace XboxLiveApp.BusinnesLayer.Bootstrapp
{
    public class BusinessNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IGameService>().To<GameService>();
            Kernel.Bind<IUserService>().To<UserService>();
            Kernel.Bind<IFriendRequestService>().To<FriendRequestService>();
            Kernel.Bind<IAchievementService>().To<AchievementService>();
            Kernel.Bind<IDatabaseInitializer>().To<DatabaseInitializer>();
            Kernel.Bind<IEmailService>().To<EmailService>();
            Kernel.Bind<ISellService>().To<SellService>();
        }
    }
}
