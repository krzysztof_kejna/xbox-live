﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace XboxLiveApp.BusinnesLayer.Services
{
    public interface IEmailService
    {
        Task SendEmail(string emailAdress, string subject, string body);
    }

    public class EmailService : IEmailService
    {
        private readonly IGameService _gameService;
        private readonly IUserService _userService;
        private bool mailSent = false;

        public EmailService(IGameService gameService, IUserService userService)
        {
            _gameService = gameService;
            _userService = userService;
        }



        public async Task SendEmail(string emailAdress, string subject, string body)
        {
            SmtpClient client = new SmtpClient("localhost");
            client.Port = 2500;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("grupa20191118@o2.pl", "Codementors01");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("grupa20191118@o2.pl");
            mailMessage.To.Add(emailAdress);
            mailMessage.Body = body;
            mailMessage.Subject = subject;
            client.Send(mailMessage);
        }
    }
}
