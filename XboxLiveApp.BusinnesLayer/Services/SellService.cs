﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XboxLiveApp.DataLayer;
using XboxLiveApp.DataLayer.Models;

namespace XboxLiveApp.BusinnesLayer.Services
{
    public interface ISellService
    {
        Task<int> SellGame(int userId, int gameId);
        Task<List<SellDetails>> GetAllUsersThatBoughtTheGame(int gameId);
    }

    public class SellService : ISellService
    {
        private readonly Func<IXboxLiveAppDbContext> _dbContextFactory;
        private readonly IGameService _gameService;
        private readonly IUserService _userService;
        private readonly IEmailService _emailService;
        private readonly IAchievementService _achievementService;

        public SellService(Func<IXboxLiveAppDbContext> dbContextFactory,
            IGameService gameService,
            IUserService userService,
            IEmailService emailService,
            IAchievementService achievementService)
        {
            _dbContextFactory = dbContextFactory;
            _gameService = gameService;
            _userService = userService;
            _emailService = emailService;
            _achievementService = achievementService;
        }

        public async Task<int> SellGame(int userId, int gameId)
        {
            var choosenUser = await _userService.GetUserById(userId);
            var choosenGame = await _gameService.GetGameById(gameId);

            if (choosenUser == null)
            {
                throw new Exception($"There is no user with ID {userId}");
            }

            if (choosenGame == null)
            {
                throw new Exception($"There is no game with ID {gameId}");
            }

            var userAge = DateTime.Now - choosenUser.DOB;

            if (Math.Floor(userAge.Value.TotalDays / 365) < choosenGame.AgeRestriction)
            {
                throw new Exception("You are to young to buy this game. Szczylu.");
            }

            var newSell = new SellDetails
            {
                UserId = choosenUser.Id,
                GameId = choosenGame.Id,
                Name = choosenGame.Name,
                Price = choosenGame.Price
            };

            var achievementList = await _achievementService.GetAllAchievmentForGame(gameId);

            using (var context = _dbContextFactory())
            {
                var sellEntity = context.SellDetails.Add(newSell);

                foreach (var achievment in achievementList)
                {
                    var newAchievement = new Achievement
                    {
                        Name = achievment.Name,
                        Description = achievment.Description,
                        Score = achievment.Score,
                        AchievedByUser = false,
                        GameId = gameId,
                        UserId = userId
                    };

                    context.Achievements.Add(newAchievement);
                }
                await context.SaveChangesAsync();
                return sellEntity.Entity.Id;
            }
        }


        public async Task<List<SellDetails>> GetAllUsersThatBoughtTheGame(int gameId)
        {
            using (var context = _dbContextFactory())
            {
                var gameList = await context.SellDetails.Where(g => g.GameId == gameId)
                    .ToListAsync();
                return gameList;
            }
        }

}
}