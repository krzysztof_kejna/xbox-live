﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XboxLiveApp.DataLayer;
using XboxLiveApp.DataLayer.Models;

namespace XboxLiveApp.BusinnesLayer.Services
{
    public interface IFriendRequestService
    {
        Task<int> AddRequest(FriendRequest request);

        Task<List<FriendRequest>> GetRequests(string nick);
    }

    public class FriendRequestService : IFriendRequestService
    {
        private readonly Func<IXboxLiveAppDbContext> _dbContextFactory;

        public FriendRequestService(Func<IXboxLiveAppDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<int> AddRequest(FriendRequest request)
        {
            using (var context = _dbContextFactory())
            {
                var addedRequest = context.Requests.Add(request);
                await context.SaveChangesAsync();

                return addedRequest.Entity.Id;
            }
        }

        public async Task<List<FriendRequest>> GetRequests(string nick)
        {
            using (var context = _dbContextFactory())
            {
                var requests = await context.Requests.
                    Where(request => request.IsActive == true && request.Sender.Nick == nick)
                    .ToListAsync();
                return requests;
            }
        }
    }
}