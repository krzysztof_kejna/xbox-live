﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XboxLiveApp.DataLayer;
using XboxLiveApp.DataLayer.Models;

namespace XboxLiveApp.BusinnesLayer.Services
{
    public interface IAchievementService
    {
        Task<int> Add(Achievement achievement);
        Task<int> GetAchievmentScoreForGame(int gameId);
        Task ChangeAchievmentStatus(int achievementId, int userId);
        Task<List<Achievement>> ReturnAllAchieventsAchievedForUser(int userId);
        Task<List<Achievement>> GetAllAchievmentForGame(int gameId);
    }
    public class AchievementService : IAchievementService
    {
        private readonly Func<IXboxLiveAppDbContext> _dbContextFactory;

        public AchievementService(Func<IXboxLiveAppDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<int> Add(Achievement achievement)
        {
            using (var context = _dbContextFactory())
            {
                var addedAchievementEntity = context.Achievements.Add(achievement);
                await context.SaveChangesAsync();

                return addedAchievementEntity.Entity.Id;
            }
        }

        public async Task<int> GetAchievmentScoreForGame(int gameId)
        {
            int result = 0;

            var score = await GetAllAchievmentForGame(gameId);
            foreach (var achievement in score)
            {
                result += achievement.Score;
            }

            return result;

        }

        public async Task<List<Achievement>> GetAllAchievmentForGame(int gameId)
        {
            using (var context = _dbContextFactory())
            {
                var achievmentList = await context.Achievements
                    .Where(g => g.GameId == gameId && g.UserId == null)
                    .ToListAsync();
                return achievmentList;
            }
        }

        public async Task ChangeAchievmentStatus(int achievementId, int userId)
        {
            var getAchievements = ReturnAllAchieventsAvaliableForUser(userId).Result;
            var achievement = getAchievements.FirstOrDefault(a => a.Id == achievementId);
            
            if (achievement == null)
            {
                throw new Exception("This user cant get achievment for this game");
            }

            achievement.AchievedByUser = true;

            using (var context = _dbContextFactory())
            {
                context.Achievements.Update(achievement);
                await context.SaveChangesAsync();
            }
        }
        
        private async Task<List<Achievement>> ReturnAllAchieventsAvaliableForUser(int userId)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Achievements.Include(u => u.User)
                    .Where(a => a.AchievedByUser == false)
                    .Where(u => u.User.Id == userId).ToListAsync();
            }
        }

        public async Task<List<Achievement>> ReturnAllAchieventsAchievedForUser(int userId)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Achievements.Include(u => u.User)
                    .Where(a => a.AchievedByUser == true)
                    .Where(u => u.User.Id == userId).ToListAsync();
            }
        }
    }
}
