﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XboxLiveApp.DataLayer;
using XboxLiveApp.DataLayer.Models;

namespace XboxLiveApp.BusinnesLayer.Services
{
    public interface IUserService
    {
        Task<int> AddUser(Users user);
        Task<Users> GetUserById(int id);
        Task GamePassForUser(Users user);

        Task<List<Users>> GetUsers();
        Task<List<Users>> GetUsersWithActiveGamePass();
    }

    public class UserService : IUserService
    {
        private readonly Func<IXboxLiveAppDbContext> _dbContextFactory;

        public UserService(Func<IXboxLiveAppDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<int> AddUser(Users user)
        {
            if (user.Password.Length < 6)
            {
                throw new Exception("Password was too short");
            }

            using (var context = _dbContextFactory())
            {
                var addedUserEntity = context.Users.Add(user);
                await context.SaveChangesAsync();

                return addedUserEntity.Entity.Id;
            }
        }

        public async Task<Users> GetUserById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Users.FirstOrDefaultAsync(u => u.Id == id);
            }
        }

        public async Task<List<Users>> GetUsersWithActiveGamePass()
        {
            using (var context = _dbContextFactory())
            {
                var users = context.Users.
                    Where(user => user.GPS == true)
                    .ToListAsync();
                return await users;
            }
        }

        public async Task GamePassForUser(Users user)
        {
            using (var context = _dbContextFactory())
            {
                context.Users.Update(user);
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<Users>> GetUsers()
        {
            using (var context = _dbContextFactory())
            {
                return await context.Users.ToListAsync();
            }
        }
    }
}