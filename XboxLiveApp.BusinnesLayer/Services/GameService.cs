﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using XboxLiveApp.DataLayer;
using XboxLiveApp.DataLayer.Models;

namespace XboxLiveApp.BusinnesLayer.Services
{
    public interface IGameService
    {
        Task<int> AddGame(Game game);
        Task ModifyGame(Game game);
        Task<List<Game>> GetGames();
        Task<Game> GetGameById(int id);
        Task<string> IsGameAvalibleForUser(int userId, int gameId);
    }

    public class GameService : IGameService
    {
        private readonly Func<IXboxLiveAppDbContext> _dbContextFactory;
        private readonly IUserService _userService;

        public GameService(Func<IXboxLiveAppDbContext> dbContextFactory, IUserService userService)
        {
            _dbContextFactory = dbContextFactory;
            _userService = userService;
        }

        public async Task<int> AddGame(Game game)
        {
            using (var context = _dbContextFactory())
            {
                var addedGameEntity = context.Games.Add(game);
                await context.SaveChangesAsync();

                return addedGameEntity.Entity.Id;
            }
        }

        public async Task ModifyGame(Game game)
        {
            using (var context = _dbContextFactory())
            {
                context.Games.Update(game);
                await context.SaveChangesAsync();
            }
        }

        public async Task<List<Game>> GetGames()
        {
            using (var context = _dbContextFactory())
            {
                return await context.Games.ToListAsync();
            }
        }

        public async Task<Game> GetGameById(int id)
        {
            using (var context = _dbContextFactory())
            {
                return await context.Games.FirstOrDefaultAsync(x => x.Id == id);
            }
        }


        public async Task<string> IsGameAvalibleForUser(int userId, int gameId)
        {
            var user = await _userService.GetUserById(userId);
            var game = await GetGameById(gameId);

            if (user.GPS == true && game.GamePass == true)
            {
                return "You have access to this game by gamepas";
            }

            using (var context = _dbContextFactory())
            {
                var gameBought = await context.SellDetails.FirstOrDefaultAsync(s => s.GameId == gameId && s.UserId == userId);

                if (gameBought != null)
                {
                    return "You have bought this game";
                }

                return "you have no access to this game";
            }
        }
    }
}